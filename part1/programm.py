import sys

def arrays(array1, array2, place):
    array = array1[:place] + array2 + array1[place:]
    return array

array1 = list(sys.argv[1].split())
array2 = list(sys.argv[2].split())
place = int(sys.argv[3])
result = arrays(array1, array2, place)

print(result)


